# Angular example CRUD app
## Prerequisites 
- Python 2.7^
- Node.js 

## Run
```
    npm i
    gulp
```

## Should get output
```
    > gulp
    [20:22:41] Using gulpfile E:\@dev\ng-example\gulpfile.js
    [20:22:41] Starting 'serve'...
    [BS] Access URLs:
     ---------------------------------------
           Local: http://localhost:888
        External: http://10.252.242.126:888
     ---------------------------------------
              UI: http://localhost:3002
     UI External: http://10.252.242.126:3002
     ---------------------------------------
    [BS] Serving files from: app
    [20:22:43] Finished 'serve' after 2.32 s
    [20:22:43] Starting 'watch'...
    [20:22:43] Finished 'watch' after 279 ms
    [20:22:43] Starting 'default'...
    [20:22:43] Finished 'default' after 5.56 ?s
```