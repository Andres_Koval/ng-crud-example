angular.module('myApp').controller('ContactController', ['$scope','$routeParams','cs',function ($scope,$routeParams,cs) {
	$scope.newcontact = {};
	$scope.contacts = cs.list();
	$scope.saveContact = function () {
		if ($scope.newcontact && $scope.newcontact.name){
			cs.save($scope.newcontact);
			$scope.newcontact = {};
		}
	};
	$scope.edit = function (id) {
		$scope.newcontact = angular.copy(cs.get(id));
	};

	if ($routeParams.id){
		$scope.edit($routeParams.id);
	}
}]);
