var app = angular.module('myApp', ['ngRoute', 'ngAnimate', 'ngMaterial']);

app
	.value('version', '0.1')
	.config(['$routeProvider', function ($routeProvider) {
		$routeProvider
			.when('/contact', {
				templateUrl: 'view1/contact.html',
				controller: 'ContactController'
			})
			.when('/contact/:id', {
				templateUrl: 'view1/contact.html',
				controller: 'ContactController'
			})
			.when('/list', {
				templateUrl: 'view2/list.html',
				controller: 'DetailsController'
			})
			.otherwise({redirectTo: '/list'})
		;
	}])
;

app
	.config(['$mdThemingProvider', function ($mdThemingProvider) {
		$mdThemingProvider
			.theme('default')
			//.primaryPalette('grey')
			.accentPalette('red')
		;
		$mdThemingProvider.theme('docs-dark', 'default')
			.primaryPalette('yellow')
			.dark()
		;
	}]);
