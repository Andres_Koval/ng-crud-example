angular.module('myApp').service('cs', ['$interval', '$log', function ($interval, $log) {
	var contacts = [{
		id: _.uniqueId(),
		name: 'John Smith',
		email: 'a@a.aa',
		mobile: '12345'
	}];
	this.selected = 0;

	this.save = function (c) {
		if (c.id == null) {
			c.id = _.uniqueId();
			contacts.push(c);
		} else {
			var i = _.findIndex(contacts, {id: c.id});
			contacts[i] = c;
		}
		$log.debug('saving', c)
	};

	this.get = function (id) {
		var c = _.find(contacts, {id: id});
		$log.debug('get', id, c, typeof id);
		return c;
	};

	this.delete = function (id) {
		var i = _.findIndex(contacts, {id: id});
		var c = contacts.splice(i, 1);
		$log.debug('delete', id, i, c);
	};

	this.list = function () {
		return contacts;
	}
}]);