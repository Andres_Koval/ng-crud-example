angular.module('myApp').controller('DetailsController', ['$scope','$location','cs',function ($scope,$location,cs) {
	$scope.contacts = cs.list();
	$scope.delete = function (id) {
		cs.delete(id);
	};
	$scope.edit = function (id) {
		$location.path('/contact/'+id);
	}
}]);

