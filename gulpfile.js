var gulp = require('gulp');
var browserSync = require('browser-sync');

gulp.task('serve', function (done) {
	browserSync({
		open: false,
		port: 888,
		browser: "google chrome",
		server: {
			baseDir: "app",
			index: 'index.html'
		}
	}, done);
});

gulp.task('watch', ['serve'], function () {
	gulp.watch(['app/**/*.js', 'app/**/*.html'], browserSync.reload);
});

gulp.task('default', ['watch']);

